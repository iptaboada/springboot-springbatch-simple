package com.springboot.springbatch.app.batch.listener;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemWriteListener;

import com.springboot.springbatch.app.batch.domain.CreditCardRisk;

public class CreditCardItemWriterListener implements ItemWriteListener<CreditCardRisk>{

	private static final Logger LOGGER = LoggerFactory.getLogger(CreditCardItemWriterListener.class);
	
	@Override
	public void beforeWrite(List<? extends CreditCardRisk> items) {
		LOGGER.info("Before Write");
		
	}

	@Override
	public void afterWrite(List<? extends CreditCardRisk> items) {
		for(CreditCardRisk creditCardRisk:items) {
			LOGGER.info("afterWrite"+creditCardRisk.toString());
		}
	}

	@Override
	public void onWriteError(Exception exception, List<? extends CreditCardRisk> items) {
		LOGGER.info("onWriteError");	
	}

}
