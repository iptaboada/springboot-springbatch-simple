package com.springboot.springbatch.app.batch;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.task.configuration.EnableTask;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

@EnableTask
@SpringBootApplication
@EnableScheduling
public class PrincipalSpringBatch {

	@Autowired
	Job job;
	
	@Autowired
	JobLauncher jobLauncher;
	
	public static void main(String[] args) {
		SpringApplication.run(PrincipalSpringBatch.class, args);
	}
	
    @Scheduled(fixedRate = 5000)
    public void perform() throws Exception 
    {
        JobParameters params = new JobParametersBuilder()
                .addString("JobID", String.valueOf(System.currentTimeMillis()))
                .toJobParameters();
        jobLauncher.run(job, params);
    }
}
