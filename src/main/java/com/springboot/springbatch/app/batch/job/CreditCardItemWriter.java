package com.springboot.springbatch.app.batch.job;

import java.util.List;

import org.springframework.batch.item.ItemWriter;
import org.springframework.beans.factory.annotation.Autowired;

import com.springboot.springbatch.app.batch.domain.CreditCardRisk;
import com.springboot.springbatch.app.batch.repository.CreditCardRiskRepository;



public class CreditCardItemWriter implements ItemWriter<CreditCardRisk>{

	@Autowired
	private CreditCardRiskRepository repository;
	
	@Override
	public void write(List<? extends CreditCardRisk> list) throws Exception {
		//Guardamos los elementos leidos en su destino
		repository.saveAll(list);
		
	}
}
