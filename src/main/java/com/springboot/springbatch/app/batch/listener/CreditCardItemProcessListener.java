package com.springboot.springbatch.app.batch.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemProcessListener;

import com.springboot.springbatch.app.batch.domain.CreditCard;
import com.springboot.springbatch.app.batch.domain.CreditCardRisk;

public class CreditCardItemProcessListener implements ItemProcessListener<CreditCard, CreditCardRisk>{

	private static final Logger LOGGER = LoggerFactory.getLogger(CreditCardItemProcessListener.class);
	
	@Override
	public void beforeProcess(CreditCard item) {
		LOGGER.info("Before process");
		
	}

	@Override
	public void afterProcess(CreditCard creditCard, CreditCardRisk CreditCardRisk) {
		LOGGER.info("AfterProcess: " + creditCard + "-->" + CreditCardRisk);
		
	}

	@Override
	public void onProcessError(CreditCard item, Exception e) {
		LOGGER.info("onProcessError");		
	}
}
