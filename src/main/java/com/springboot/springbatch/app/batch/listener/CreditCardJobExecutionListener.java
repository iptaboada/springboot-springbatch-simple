package com.springboot.springbatch.app.batch.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobExecutionListener;

public class CreditCardJobExecutionListener implements JobExecutionListener {

	private static final Logger LOGGER = LoggerFactory.getLogger(CreditCardJobExecutionListener.class);

	@Override
	public void beforeJob(JobExecution jobExecution) {
		LOGGER.info("Before Job");
		
	}

	@Override
	public void afterJob(JobExecution jobExecution) {
		LOGGER.info("After Job");
		
	}
	
}
