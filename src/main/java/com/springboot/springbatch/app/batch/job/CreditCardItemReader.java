package com.springboot.springbatch.app.batch.job;

import java.util.Iterator;

import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.annotation.BeforeStep;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.NonTransientResourceException;
import org.springframework.batch.item.ParseException;
import org.springframework.batch.item.UnexpectedInputException;
import org.springframework.beans.factory.annotation.Autowired;

import com.springboot.springbatch.app.batch.domain.CreditCard;
import com.springboot.springbatch.app.batch.repository.CreditCardRepository;

public class CreditCardItemReader implements ItemReader<CreditCard> {

	@Autowired
	private CreditCardRepository repository;
	
	private Iterator<CreditCard> usersIterator;
	
	@BeforeStep
	public void before(StepExecution stepExecution) {
		usersIterator = repository.findAll().iterator();
	}
	
	@Override
	public CreditCard read() throws Exception, UnexpectedInputException, ParseException, NonTransientResourceException {
		//Realizar el recorrido del iterador obtenido
		if(usersIterator != null && usersIterator.hasNext()) {
			return usersIterator.next();
		}else {
			return null;
		}
	}
}
