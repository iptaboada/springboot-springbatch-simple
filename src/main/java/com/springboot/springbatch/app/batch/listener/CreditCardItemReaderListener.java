package com.springboot.springbatch.app.batch.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemReadListener;

import com.springboot.springbatch.app.batch.domain.CreditCard;

public class CreditCardItemReaderListener implements ItemReadListener<CreditCard>{
	
	private static final Logger LOGGER =  LoggerFactory.getLogger(CreditCardItemReaderListener.class);

	@Override
	public void beforeRead() {
		LOGGER.info("beforeRead");
		
	}

	@Override
	public void afterRead(CreditCard creditCard) {
		LOGGER.info("afterRead: " + creditCard);
		
	}

	@Override
	public void onReadError(Exception ex) {
		LOGGER.info("onReadError");
		
	}

}
