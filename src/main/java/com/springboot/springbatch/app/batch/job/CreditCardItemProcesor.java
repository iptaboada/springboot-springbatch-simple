package com.springboot.springbatch.app.batch.job;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import org.springframework.batch.item.ItemProcessor;

import com.springboot.springbatch.app.batch.domain.CreditCard;
import com.springboot.springbatch.app.batch.domain.CreditCardRisk;

import static java.time.temporal.ChronoUnit.DAYS;

public class CreditCardItemProcesor implements ItemProcessor<CreditCard, CreditCardRisk> {

	@Override
	public CreditCardRisk process(CreditCard item) throws Exception {
		
		LocalDate today = LocalDate.now();
		LocalDate lastDate = item.getLastPay().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		long daysBetween = DAYS.between(today, lastDate);
		
		int risk;
		if(daysBetween >= 20) {
			risk = CreditCardRisk.HIGH;
		}else if(daysBetween > 10) {
			risk = CreditCardRisk.LOW;
		}else {
			risk = CreditCardRisk.NORMAL;
		}

		//Creamos y devolvemos el riesgo a insertar
		CreditCardRisk creditCardRisk = new CreditCardRisk(new Date(),risk,item);
		return creditCardRisk;
	}

}
