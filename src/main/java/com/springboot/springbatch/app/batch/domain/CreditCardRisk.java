package com.springboot.springbatch.app.batch.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class CreditCardRisk {

	public static final int HIGH = 3;
	public static final int LOW = 2;
	public static final int NORMAL = 1;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private Date date;
	private int risk;
	
	@OneToOne
	private CreditCard creditCard;
	
	public CreditCardRisk() {}
	
	public CreditCardRisk(Date date, int risk, CreditCard creditCard) {
		this.date = date;
		this.risk = risk;
		this.creditCard = creditCard;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public int getRisk() {
		return risk;
	}

	public void setRisk(int risk) {
		this.risk = risk;
	}

	public CreditCard getCreditCard() {
		return creditCard;
	}

	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}

	
	
	
}
