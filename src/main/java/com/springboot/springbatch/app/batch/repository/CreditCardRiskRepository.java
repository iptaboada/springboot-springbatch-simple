package com.springboot.springbatch.app.batch.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.springboot.springbatch.app.batch.domain.CreditCardRisk;

@Repository
public interface CreditCardRiskRepository extends JpaRepository<CreditCardRisk, Long>{

}
