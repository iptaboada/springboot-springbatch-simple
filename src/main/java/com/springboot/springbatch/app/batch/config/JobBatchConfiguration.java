package com.springboot.springbatch.app.batch.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.step.tasklet.TaskletStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.springboot.springbatch.app.batch.domain.CreditCard;
import com.springboot.springbatch.app.batch.domain.CreditCardRisk;
import com.springboot.springbatch.app.batch.job.CreditCardItemProcesor;
import com.springboot.springbatch.app.batch.job.CreditCardItemReader;
import com.springboot.springbatch.app.batch.job.CreditCardItemWriter;
import com.springboot.springbatch.app.batch.listener.CreditCardItemProcessListener;
import com.springboot.springbatch.app.batch.listener.CreditCardItemReaderListener;
import com.springboot.springbatch.app.batch.listener.CreditCardItemWriterListener;
import com.springboot.springbatch.app.batch.listener.CreditCardJobExecutionListener;

@Configuration
@EnableBatchProcessing
public class JobBatchConfiguration {

	@Autowired
	public JobBuilderFactory jobBuilderFactory;
	
	@Autowired
	public StepBuilderFactory stepBuilderFactory;
		
	@Bean
	public CreditCardItemReader reader() {
		return new CreditCardItemReader();
	}
	
	@Bean
	public CreditCardItemProcesor procesor() {
		return new CreditCardItemProcesor();
	}
	
	@Bean
	public CreditCardItemWriter writer() {
		return new CreditCardItemWriter();
	}
	
	@Bean
	public CreditCardItemReaderListener creditCardItemReaderListener() {
		return new CreditCardItemReaderListener();
	}
	
	@Bean
	public CreditCardItemProcessListener creditCardProcessListener() {
		return new CreditCardItemProcessListener();
	}
	
	@Bean
	public CreditCardItemWriterListener creditCardItemWriterListener() {
		return new CreditCardItemWriterListener();
	}
	
	@Bean
	public CreditCardJobExecutionListener creditCardJobExecutionListener() {
		return new CreditCardJobExecutionListener();
	}
	
	@Bean
	public Job job(Step step,CreditCardJobExecutionListener jobExecutionListener) {
		Job job= jobBuilderFactory.get("job1")
				.listener(jobExecutionListener)
				.flow(step)
				.end()
				.build();
		return job;
	}
	
	
	@Bean
	public Step step(CreditCardItemReader reader,
			CreditCardItemWriter writer,
			CreditCardItemProcesor procesor,
			CreditCardItemReaderListener readerListener,
			CreditCardItemProcessListener creditCardItemProcessListener,
			CreditCardItemWriterListener writeListener) {
		
		TaskletStep step = stepBuilderFactory.get("step1")
				.<CreditCard,CreditCardRisk>chunk(100)
				.reader(reader)
				.processor(procesor)
				.writer(writer)
				.listener(readerListener)
				.listener(creditCardItemProcessListener)
				.listener(writeListener)
				.build();
		return step;
	}
	
	@Bean
	public Step step2(CreditCardItemReader reader,
			CreditCardItemWriter writer,
			CreditCardItemProcesor procesor,
			CreditCardItemReaderListener readerListener,
			CreditCardItemProcessListener creditCardItemProcessListener,
			CreditCardItemWriterListener writeListener) {
		
		TaskletStep step = stepBuilderFactory.get("step1")
				.<CreditCard,CreditCardRisk>chunk(100)
				.reader(reader)
				.processor(procesor)
				.writer(writer)
				.listener(readerListener)
				.listener(creditCardItemProcessListener)
				.listener(writeListener)
				.build();
		return step;
		
	}
	
	
	
}
