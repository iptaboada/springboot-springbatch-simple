package com.springboot.springbatch.app.batch.repository;

import org.springframework.stereotype.Repository;

import com.springboot.springbatch.app.batch.domain.CreditCard;

import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface CreditCardRepository extends JpaRepository<CreditCard,Long>{
	
}



